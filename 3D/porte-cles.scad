module poisson() {

linear_extrude(height = 2.5)
   import (file = "poisson_shape.dxf");
}

module poisson2() {

linear_extrude(height = 4)
   import (file = "poisson_inside_pattern.dxf");
}

$fs = 1;
scale (1.5) difference() {
   difference () {
       poisson();
       translate([9.7,4,0])
           cylinder(h=6,r=2);
   };
   translate ([2.3,2.3,1.5])
       poisson2();
}
