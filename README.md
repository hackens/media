Pour compiler svg vers dxf : d'abord svg vers ps (inkscape), puis :
pstoedit -dt -f "dxf: -polyaslines -mm" in.ps out.dxf

This repo is under Creative Commons BY license.
